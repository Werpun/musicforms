﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using MusicForms.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace MusicForms.Providers
{
    public class AuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            var db = new MusicFormsDatabaseEntities();

            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            var user = db.Users.FirstOrDefault(e => e.Email == context.UserName && e.Password == context.Password);
            if (user != null)
            {
                if (user.PermissionId == 1)
                    identity.AddClaim(new Claim(ClaimTypes.Role, "admin"));
                else if (user.PermissionId == 2)
                    identity.AddClaim(new Claim(ClaimTypes.Role, "teacher"));
                else identity.AddClaim(new Claim(ClaimTypes.Role, "student"));

                var ticket = new AuthenticationTicket(identity, null);
                context.Validated(ticket);
            }
            else
            {
                context.SetError("invalid_grant", "The user name or password is incorrect.");
                return;
            }
        }
    }
}