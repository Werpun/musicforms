﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using MusicForms.DTO;
using MusicForms.Models;

namespace MusicForms.Controllers
{
    public class FormsController : ApiController
    {
        private MusicFormsDatabaseEntities db = new MusicFormsDatabaseEntities();
        private readonly IPrincipal currentUser = HttpContext.Current.User;

        // GET: api/Forms
        [Authorize]
        public IHttpActionResult GetForms()
        {
            var forms = db.Forms.AsNoTracking()
                .Select(f => new
                {
                    f.Id,
                    f.Title,
                    f.FormType,
                    f.Created,
                    f.AvailableFrom,
                    f.AvailableTo,
                    User = new
                    {
                        f.User.Id,
                        f.User.Email,
                        f.User.FirstName,
                        f.User.LastName
                    }
                })
                .ToList();

            return Ok(forms);
        }

        // GET: api/Forms/5
        [Authorize]
        public IHttpActionResult GetForm(Guid id)
        {
            var form = db.Forms.AsNoTracking()
                .Include(f => f.Questions)
                .FirstOrDefault(f => f.Id == id);

            form.Questions = form.Questions.OrderBy(q => q.OrderNumber).ToList();

            if (form == null)
            {
                return NotFound();
            }

            return Ok(form);
        }

        [HttpGet]
        [Authorize]
        [Route("api/formsByGroup")]
        public IHttpActionResult GetFormsByGroup(Guid groupId, Guid userId)
        {
            if (currentUser.IsInRole("student"))
            {
                var studentForms = db.Forms.AsNoTracking()
                    .Where(f => f.GroupId == groupId)
                    .OrderByDescending(f => f.AvailableFrom)
                    .Select(f => new
                    {
                        f.Id,
                        f.Title,
                        f.FormTypeId,
                        f.Created,
                        f.AvailableFrom,
                        f.AvailableTo,
                        f.User,
                        IsSubmitted = db.Answers.Where(a => a.UserId == userId && a.FormId == f.Id).Count() > 0
                    })
                    .ToList();

                var dateNow = DateTime.Now;

                for (int i = 0; i < studentForms.Count; i++)
                {
                    var form = studentForms[i];
                    var daysFrom = form.AvailableFrom.HasValue ? (form.AvailableFrom.Value - dateNow).TotalDays : 0;
                    var daysTo = form.AvailableTo.HasValue ? (form.AvailableTo.Value - dateNow).TotalDays : 0;

                    if (daysFrom > 7 || daysTo < -7)
                    {
                        studentForms.Remove(form);
                    }
                }

                return Ok(studentForms);
            }

            var forms = db.Forms.AsNoTracking()
                    .Where(f => f.GroupId == groupId)
                    .OrderByDescending(f => f.AvailableFrom)
                    .Select(f => new
                    {
                        f.Id,
                        f.Title,
                        f.FormTypeId,
                        f.Created,
                        f.AvailableFrom,
                        f.AvailableTo,
                        f.User
                    })
                    .ToList();

            return Ok(forms);
        }

        [HttpGet]
        [Authorize]
        [Route("api/formStats/{id}")]
        public IHttpActionResult GetFormStatistics(Guid id)
        {
            var form = db.Forms.AsNoTracking()
                .Include("Answers")
                .Where(f => f.Id == id)
                .Select(f => new
                {
                    f.Id,
                    f.Title,
                    f.FormTypeId,
                    Questions = f.Questions.OrderBy(q => q.OrderNumber),
                    Answers = f.Answers.Where(a => a.User.PermissionId == 3),
                    f.AvailableTo
                })
                .FirstOrDefault();

            if (form == null) return NotFound();

            if (currentUser.IsInRole("student"))
            {
                if (form.AvailableTo.HasValue && form.AvailableTo.Value > DateTime.Now)
                    return BadRequest("Form has not closed. You cannot see statistics yet");
            }
            return Ok(form);
        }

        [HttpPost]
        [Authorize]
        [Route("api/createForm")]
        public IHttpActionResult CreateForm(CreateFormDTO createFormDTO)
        {
            var form = new Form(createFormDTO.FormTitle, createFormDTO.FormTypeId, DateTime.Now, null, createFormDTO.UserId, createFormDTO.GroupId);
            db.Forms.Add(form);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                if (FormExists(form.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw ex;
                }
            }

            return Ok(form);
        }

        [HttpPost]
        [Authorize]
        [Route("api/saveForm")]
        public IHttpActionResult SaveForm(Form form)
        {
            if (form.Questions == null || form.Questions.Count() == 0) return BadRequest("No questions");
            var formQuestions = form.Questions.ToList();

            foreach (var question in formQuestions)
            {
                var orderNumber = formQuestions.IndexOf(question) + 1;
                if (db.Questions.Count(q => q.Id == question.Id && q.FormId == question.FormId) > 0)
                {
                    question.OrderNumber = orderNumber;
                    db.Entry(question).State = EntityState.Modified;
                }
                else if (question.AvailableAnswers.Count() > 0)
                {
                    var q = new Question(form.Id, question.ContentBlockId, question.QuestionText, question.AvailableAnswers, question.CorrectAnswer, orderNumber);
                    db.Questions.Add(q);
                }
            }

            var questionsToDelete = db.Questions.Where(q => q.FormId == form.Id).AsEnumerable();
            if (questionsToDelete.Count() > 0)
                foreach (var question in questionsToDelete)
                {
                    if (!formQuestions.Contains(question))
                        db.Questions.Remove(question);
                }

            var formFromDb = db.Forms.Find(form.Id);
            formFromDb.Title = form.Title;
            formFromDb.AvailableFrom = form.AvailableFrom ?? DateTime.Now;
            formFromDb.AvailableTo = form.AvailableTo;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FormExists(form.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok();
        }

        // DELETE: api/Forms/5
        [Authorize]
        public IHttpActionResult DeleteForm(Guid id)
        {
            var form = db.Forms.Find(id);
            if (form == null) return NotFound();

            var questions = form.Questions;
            if (questions.Count > 0)
            {
                foreach (var question in questions)
                {
                    var answers = db.Answers.Where(a => a.QuestionId == question.Id);
                    if (answers.Count() > 0)
                        db.Answers.RemoveRange(answers);
                }
                db.Questions.RemoveRange(questions);
            }

            db.Forms.Remove(form);
            db.SaveChanges();

            return Ok(form);
        }

        [HttpPost]
        [Authorize]
        [Route("api/submitForm")]
        public IHttpActionResult SubmitForm([FromBody]SubmitFormDTO submitFormDTO)
        {
            if (db.Answers.Where(a => a.UserId == submitFormDTO.UserId && a.FormId == submitFormDTO.FormId).Count() > 0)
                return BadRequest("Form is already submitted");

            var correctAnswersFromDb = db.Questions
               .Where(q => q.FormId == submitFormDTO.FormId)
               .Select(q => new
               {
                   q.Id,
                   q.CorrectAnswer
               })
               .ToList();

            var user = db.Users.Find(submitFormDTO.UserId);

            var score = 0;

            foreach (var questionAnswer in submitFormDTO.QuestionAnswerList)
            {
                var contentBlockName = db.Questions
                    .Include(q => q.ContentBlock)
                    .FirstOrDefault(q => q.Id == questionAnswer.QuestionId)
                    .ContentBlock.Name;

                var correctAnswer = correctAnswersFromDb
                    .Find(ca => ca.Id == questionAnswer.QuestionId)
                    .CorrectAnswer.Split(';')
                    .ToList();

                if (contentBlockName.Equals("Scales"))
                {
                    var temp = correctAnswer[0].Split(' ').ToList();
                    temp.RemoveAt(0);
                    temp.RemoveAt(temp.Count - 1);
                    var corAns = string.Join(" ", temp);
                    correctAnswer[0] = corAns;
                }
                var givenAnswer = questionAnswer.Answer.Split(';').ToList();

                var isCorrect = false;
                bool containsAllElements = correctAnswer.OrderBy(a => a).SequenceEqual(givenAnswer.OrderBy(a => a));
                if (containsAllElements)
                {
                    isCorrect = true;
                    score++;
                }

                if (user.PermissionId == 3)
                {
                    var existingAnswer = db.Answers
                        .FirstOrDefault(a => a.FormId == submitFormDTO.FormId
                                && a.UserId == submitFormDTO.UserId
                                && a.QuestionId == questionAnswer.QuestionId);

                    if (existingAnswer != null)
                    {
                        if (existingAnswer.GivenAnswer != questionAnswer.Answer)
                        {
                            existingAnswer.GivenAnswer = questionAnswer.Answer;
                            existingAnswer.IsCorrect = isCorrect;
                        }
                    }
                    else
                    {
                        var answer = new Answer(submitFormDTO.FormId, submitFormDTO.UserId, questionAnswer.QuestionId, questionAnswer.Answer, isCorrect);
                        db.Answers.Add(answer);
                    }
                }
            }

            db.SaveChanges();
            return Ok(score);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FormExists(Guid id)
        {
            return db.Forms.Count(e => e.Id == id) > 0;
        }
    }
}