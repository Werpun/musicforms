﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Description;
using MusicForms.DTO;
using MusicForms.Models;
using OfficeOpenXml;

namespace MusicForms.Controllers
{
    public class GroupsController : ApiController
    {
        private MusicFormsDatabaseEntities db = new MusicFormsDatabaseEntities();

        // GET: api/Groups
        [Authorize]
        public IQueryable<Group> GetGroups()
        {
            return db.Groups;
        }

        // GET: api/Groups/5
        [Authorize]
        public IHttpActionResult GetGroup(Guid id)
        {
            var group = db.Groups.AsNoTracking()
                .Where(g => g.Id == id)
                .Select(g => new
                {
                    g.Id,
                    g.Name,
                    g.TeacherId,
                    UsersCount = g.Users.Count()
                })
                .FirstOrDefault();

            if (group == null) return NotFound();

            return Ok(group);
        }

        [HttpGet]
        [Authorize]
        [Route("api/groupsByUserId")]
        public IHttpActionResult GetGroupsByUserId(Guid id)
        {
            if (Guid.Empty.Equals(id) || id == null)
                return BadRequest();

            var user = db.Users.AsNoTracking()
                .Where(u => u.Id == id).FirstOrDefault();

            if (user.PermissionId == 1)
            {
                var groupsForAdmin = db.Groups.AsNoTracking()
                .Select(g => new
                {
                    g.Id,
                    g.Name,
                    Forms = db.Forms.Count(f => f.GroupId == g.Id),
                    Members = g.Users.Where(u => u.PermissionId == 3).Count(),
                    Teacher = db.Users.Where(u => u.Id == g.TeacherId)
                    .Select(t => new
                    {
                        t.Id,
                        t.FirstName,
                        t.LastName
                    })
                })
                .ToList();

                return Ok(groupsForAdmin);
            }

            var groups = db.Groups.AsNoTracking()
                .Where(g => g.TeacherId == id || g.Users.Any(u => u.Id == id))
                .Select(g => new
                {
                    g.Id,
                    g.Name,
                    Forms = db.Forms.Count(f => f.GroupId == g.Id),
                    Members = g.Users.Where(u => u.PermissionId == 3).Count(),
                    Teacher = db.Users.Where(u => u.Id == g.TeacherId)
                    .Select(t => new
                    {
                        t.Id,
                        t.FirstName,
                        t.LastName
                    })
                })
                .ToList();

            return Ok(groups);
        }

        // PUT: api/Groups/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutGroup(Guid id, Group group)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != group.Id)
            {
                return BadRequest();
            }

            db.Entry(group).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GroupExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Groups
        [Authorize]
        [ResponseType(typeof(Group))]
        public IHttpActionResult PostGroup(Group group)
        {
            if (group.TeacherId == null || Guid.Empty.Equals(group.TeacherId))
                return BadRequest("Wrong teacher");

            group.Id = Guid.NewGuid();
            db.Groups.Add(group);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (GroupExists(group.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Ok(group);
        }

        // DELETE: api/Groups/5
        [Authorize]
        [ResponseType(typeof(Group))]
        public IHttpActionResult DeleteGroup(Guid id)
        {
            var group = db.Groups.Include("Users").FirstOrDefault(g => g.Id == id);
            if (group == null) return NotFound();

            group.Users.Clear();

            var groupForms = db.Forms.Include("Questions").Where(f => f.GroupId == group.Id);
            if (groupForms.Count() > 0)
            {
                foreach (var form in groupForms)
                {
                    var questions = form.Questions;
                    if (questions.Count > 0)
                    {
                        foreach (var question in questions)
                        {
                            var answers = db.Answers.Where(a => a.QuestionId == question.Id);
                            if (answers.Count() > 0)
                                db.Answers.RemoveRange(answers);
                        }
                        db.Questions.RemoveRange(questions);
                    }
                }
                db.Forms.RemoveRange(groupForms);
            }

            db.Groups.Remove(group);
            db.SaveChanges();

            return Ok(group);
        }

        [HttpGet]
        [Authorize]
        [Route("api/groupsGradesExcel")]
        public HttpResponseMessage GroupsGradesExcel(Guid groupId)
        {
            var group = db.Groups.Find(groupId);
            if (group == null) return new HttpResponseMessage(HttpStatusCode.NotFound);

            var students = db.Groups.Include(g => g.Users)
                .FirstOrDefault(g => g.Id == groupId)
                .Users.Where(u => u.PermissionId >= 3)
                .ToList();

            var forms = db.Forms.Include("Questions").Include("Group")
                .Where(f => f.GroupId == groupId && f.FormTypeId != 2)
                .ToList();

            var filename = group.Name.Trim().Replace(' ', '-').ToLower() + ".xlsx";

            var excel = new ExcelPackage();
            var worksheet = excel.Workbook.Worksheets.Add("Grades");

            int row = 3;
            int col = 1;
            foreach (var student in students)
            {
                worksheet.Cells[row++, col].LoadFromText($"{student.FirstName} {student.LastName}");
            }

            row = 1;
            col = 2;
            var totalMax = 0;
            foreach (var form in forms)
            {
                worksheet.Cells[row, col].LoadFromText(form.Title);
                var maxPoints = form.Questions.Count;
                totalMax += maxPoints;
                worksheet.Cells[row + 1, col++].LoadFromText(maxPoints.ToString());
            }
            worksheet.Cells[row, col].LoadFromText("Total");
            worksheet.Cells[row + 1, col].LoadFromText(totalMax.ToString());
            worksheet.Cells[row, col + 1].LoadFromText("Percent");

            row = 3;
            foreach (var student in students)
            {
                col = 2;
                var total = 0;
                foreach (var form in forms)
                {
                    var studentsPoints = db.Answers.Where(a => a.FormId == form.Id && a.UserId == student.Id)
                        .Count(a => a.IsCorrect == true);
                    total += studentsPoints;

                    worksheet.Cells[row, col++].LoadFromText(studentsPoints.ToString());
                }
                worksheet.Cells[row, col++].LoadFromText(total.ToString());

                float percent = ((float)total / totalMax) * 100;
                var percentString = $"{Math.Round(percent, 2).ToString()}%";
                worksheet.Cells[row++, col].Value = percentString;
            }

            worksheet.Cells[1, 1, row, 1].AutoFitColumns();

            var memoryStream = new MemoryStream();
            excel.SaveAs(memoryStream);
            worksheet.Dispose();
            excel.Dispose();

            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(memoryStream.ToArray())
            };
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = filename
            };

            return response;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool GroupExists(Guid id)
        {
            return db.Groups.Count(e => e.Id == id) > 0;
        }
    }
}