﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using MusicForms.DTO;
using MusicForms.Models;

namespace MusicForms.Controllers
{
    public class UsersController : ApiController
    {
        private MusicFormsDatabaseEntities db = new MusicFormsDatabaseEntities();

        [HttpPost]
        [Route("api/register")]
        public IHttpActionResult Register(RegisterUserDTO registerUser)
        {
            if (db.Users.Count(u => u.Email == registerUser.Email) > 0)
                return BadRequest("User exists");

            var user = new User(registerUser.FirstName, registerUser.LastName, registerUser.Email, registerUser.Password, registerUser.PermissionId);

            if (registerUser.PermissionId == 3 && registerUser.GroupId != Guid.Empty)
            {
                var group = db.Groups.FirstOrDefault(g => g.Id == registerUser.GroupId);
                if (group == null) return BadRequest("Group does not exist");
                group.Users.Add(user);
            }

            db.Users.Add(user);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (UserExists(user.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Ok(new { user.Id, user.FirstName, user.LastName, user.Email, user.PermissionId });
        }

        // GET: api/Users
        [Authorize]
        public IEnumerable<User> GetUsers()
        {
            var users = db.Users.AsNoTracking()
                .ToList();

            return users;
        }

        // GET: api/Users/5
        [Authorize]
        public IHttpActionResult GetUser(Guid id)
        {
            var user = db.Users.Where(u => u.Id == id)
                    .Select(u => new
                    {
                        u.Email,
                        u.FirstName,
                        u.LastName,
                        u.Permission,
                        Answers = u.Answers.Select(a => new
                        {
                            a.Id,
                            a.FormId,
                            Question = db.Questions.FirstOrDefault(q => q.Id == a.QuestionId).QuestionText,
                            a.GivenAnswer,
                            a.IsCorrect
                        }),
                        Groups = u.Groups.Select(g => new
                        {
                            g.Id,
                            g.Name,
                            Teacher = g.Users.Select(us => new
                            {
                                us.Id,
                                us.FirstName,
                                us.LastName
                            })
                            .FirstOrDefault(us => us.Id == g.TeacherId),
                            Forms = g.Forms.Select(f => new
                            {
                                f.Id,
                                f.Title,
                                f.Created,
                                Questions = f.Questions.Select(q => new
                                {
                                    q.Id,
                                    q.QuestionText,
                                    q.AvailableAnswers,
                                    q.CorrectAnswer
                                })
                            })
                        })
                    })
                    .FirstOrDefault();

            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        [HttpGet]
        [Authorize]
        [Route("api/user")]
        public IHttpActionResult GetUserByEmail([FromUri]string email)
        {
            if (string.IsNullOrEmpty(email))
                return BadRequest("Empty email");

            if (db.Users.Count(u => u.Email == email) == 0)
                return BadRequest("User does not exist");

            try
            {
                var user = db.Users.Where(u => u.Email == email)
                    .FirstOrDefault();

                return Ok(user);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Authorize]
        [Route("api/getStudentsByGroup")]
        public IHttpActionResult GetStudentsByGroup(Guid groupId)
        {
            if (Guid.Empty.Equals(groupId) || db.Groups.Find(groupId) == null)
                return BadRequest();

            var students = db.Users.AsNoTracking()
                .Where(u => u.Groups.Where(g => g.Id == groupId).Count() > 0 && u.PermissionId == 3)
                .ToList();

            return Ok(students);
        }


        [HttpPost]
        [Authorize]
        [Route("api/addStudentToGroup")]
        public IHttpActionResult AddStudentToGroup(StudentGroupDTO studentGroupDTO)
        {
            if (Guid.Empty.Equals(studentGroupDTO.GroupId) || Guid.Empty.Equals(studentGroupDTO.StudentId))
                return BadRequest();

            var group = db.Groups.Find(studentGroupDTO.GroupId);
            var user = db.Users.Find(studentGroupDTO.StudentId);
            if (group == null || user == null) return NotFound();

            if (group.Users.Count(u => u.Id == user.Id) > 0)
                return BadRequest("User is already signed in this group");

            group.Users.Add(user);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok("User added");
        }

        [HttpPost]
        [Authorize]
        [Route("api/removeStudentFromGroup")]
        public IHttpActionResult RemoveStudentFromGroup(StudentGroupDTO studentGroupDTO)
        {
            if (Guid.Empty.Equals(studentGroupDTO.GroupId) || Guid.Empty.Equals(studentGroupDTO.StudentId))
                return BadRequest();

            var group = db.Groups.Include("Users").FirstOrDefault(g => g.Id == studentGroupDTO.GroupId);
            var user = db.Users.Find(studentGroupDTO.StudentId);
            if (group == null || user == null) return NotFound();

            if (group.Users.Count(u => u.Id == user.Id) == 0)
                return BadRequest("User is not signed in this group");

            group.Users.Remove(user);

            var answersId = db.Answers.Include("Form").AsNoTracking()
                .Where(a => a.UserId == studentGroupDTO.StudentId)
                .Where(a => a.Form.GroupId == studentGroupDTO.GroupId)
                .Select(a => a.Id)
                .ToList();

            var answers = db.Answers.Where(a => answersId.Contains(a.Id)).ToList();
            db.Answers.RemoveRange(answers);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok("User removed");
        }

        [HttpGet]
        [Authorize]
        [Route("api/usersGradesFromGroup")]
        public IHttpActionResult UsersGradesFromGroup(Guid groupId)
        {
            var students = db.Groups.Include(g => g.Users)
                .FirstOrDefault(g => g.Id == groupId)
                .Users.Where(u => u.PermissionId == 3)
                .ToList();

            var forms = db.Forms.Include("Questions").Include("Group").Where(f => f.GroupId == groupId && f.FormTypeId != 2).ToList();
            //if (students.Count() == 0 || forms.Count() == 0) return NotFound();

            var studentsGrades = new Dictionary<Guid, List<GradesDTO>>();
            foreach (var student in students)
            {
                var list = new List<GradesDTO>();
                foreach (var form in forms)
                {
                    var studentsPoints = db.Answers.Where(a => a.FormId == form.Id && a.UserId == student.Id)
                        .Count(a => a.IsCorrect == true);

                    var maxPoints = form.Questions.Count;

                    list.Add(new GradesDTO(form.Id, studentsPoints, maxPoints));
                }
                studentsGrades.Add(student.Id, list);
            }

            return Ok(studentsGrades);
        }

        [HttpGet]
        [Authorize]
        [Route("api/userProfileStatistics/{id}")]
        public IHttpActionResult GetUserProfileStatistics(Guid id)
        {
            var list = new List<UserProfileStatisticsDTO>();
            if (db.Users.Find(id).PermissionId == 3)
            {
                var studentsGroup = db.Groups.Include("Forms").Where(g => g.Users.Any(u => u.Id == id)).ToList();
                if (studentsGroup.Count() == 0) return NotFound();

                foreach (var group in studentsGroup)
                {
                    var forms = db.Forms.Include("Questions").Include("Group")
                        .Where(f => f.GroupId == group.Id && f.FormTypeId != 2)
                        .ToList();

                    foreach (var form in forms)
                    {
                        var studentsPoints = db.Answers.Where(a => a.FormId == form.Id && a.UserId == id)
                            .Count(a => a.IsCorrect == true);

                        var maxPoints = form.Questions.Count;

                        var percent = Math.Round((double)studentsPoints / maxPoints, 2) * 100;
                        list.Add(new UserProfileStatisticsDTO(form.Title, form.Created, percent));
                    }
                }
            }
            else
            {
                var forms = db.Forms.Include("Questions")
                    .Where(f => f.UserId == id && f.FormTypeId != 2)
                    .ToList();

                foreach (var form in forms)
                {
                    var studentsPoints = db.Answers.Where(a => a.FormId == form.Id)
                        .Count(a => a.IsCorrect == true);

                    var answers = db.Answers.Where(a => a.FormId == form.Id)
                        .Select(a => new
                        {
                            a.UserId
                        });

                    var count = answers.Distinct().Count();

                    var maxPoints = count == 0 ? form.Questions.Count : form.Questions.Count * count;

                    var percent = Math.Round((double)studentsPoints / maxPoints, 2) * 100;
                    list.Add(new UserProfileStatisticsDTO(form.Title, form.Created, percent));
                }
            }

            return Ok(list);
        }

        [HttpGet]
        [Authorize]
        [Route("api/usersRoles")]
        public IHttpActionResult UserRole()
        {
            var users = db.Users.AsNoTracking().Select(u => new
            {
                u.Id,
                u.FirstName,
                u.LastName,
                u.PermissionId
            })
            .ToList();

            return Ok(users);
        }

        [HttpPost]
        [Authorize]
        [Route("api/usersRoles")]
        public IHttpActionResult UserRole(IEnumerable<UsersRolesDTO> usersRoles)
        {
            if (usersRoles.Count() == 0)
                return BadRequest();

            var users = db.Users.ToList();

            foreach (var user in usersRoles)
            {
                users.FirstOrDefault(u => u.Id == user.Id).PermissionId = user.PermissionId;
            }

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return Ok(users);
        }

        // DELETE: api/Users/5
        [Authorize]
        [ResponseType(typeof(User))]
        public IHttpActionResult DeleteUser(Guid id)
        {
            User user = db.Users.Find(id);
            if (user == null)
            {
                return NotFound();
            }

            db.Users.Remove(user);
            db.SaveChanges();

            return Ok(user);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserExists(Guid id)
        {
            return db.Users.Count(e => e.Id == id) > 0;
        }
    }
}