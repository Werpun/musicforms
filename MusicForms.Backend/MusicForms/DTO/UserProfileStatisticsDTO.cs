﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MusicForms.DTO
{
    public class UserProfileStatisticsDTO
    {
        public UserProfileStatisticsDTO(string formName, DateTime submitFormDate, double percent)
        {
            FormName = formName;
            SubmitFormDate = submitFormDate;
            Percent = percent;
        }

        public string FormName { get; set; }
        public DateTime SubmitFormDate { get; set; }
        public double Percent { get; set; }
    }
}