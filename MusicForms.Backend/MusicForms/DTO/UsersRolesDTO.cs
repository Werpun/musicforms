﻿using MusicForms.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MusicForms.DTO
{
    public class UsersRolesDTO
    {
        public UsersRolesDTO(Guid id, int permissionId)
        {
            Id = id;
            PermissionId = permissionId;
        }

        public Guid Id { get; set; }
        public int PermissionId { get; set; }
    }

}