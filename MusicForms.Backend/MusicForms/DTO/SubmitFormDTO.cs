﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MusicForms.DTO
{
    public class QuestionAnswer
    {
        [JsonProperty("questionId")]
        public long QuestionId { get; set; }
        [JsonProperty("answer")]
        public string Answer { get; set; }
    }

    public class SubmitFormDTO
    {
        public Guid FormId { get; set; }
        public Guid UserId { get; set; }
        public List<QuestionAnswer> QuestionAnswerList { get; set; }
    }
}