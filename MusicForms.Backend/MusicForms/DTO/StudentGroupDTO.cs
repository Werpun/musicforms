﻿using System;

namespace MusicForms.Controllers
{
    public class StudentGroupDTO
    {
        public Guid StudentId { get; set; }
        public Guid GroupId { get; set; }
    }
}