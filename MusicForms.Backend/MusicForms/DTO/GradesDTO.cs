﻿using MusicForms.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MusicForms.DTO
{
    public class GradesDTO
    {
        public GradesDTO(Guid formId, int studentsPoints, int maxPoints)
        {
            FormId = formId;
            StudentsPoints = studentsPoints;
            MaxPoints = maxPoints;
        }

        public Guid FormId { get; set; }
        public int StudentsPoints { get; set; }
        public int MaxPoints { get; set; }
    }

    public class StudentsGradesDTO
    {
        public StudentsGradesDTO(User student, List<GradesDTO> grades)
        {
            Student = student;
            Grades = grades;
        }

        public User Student { get; set; }
        public List<GradesDTO> Grades { get; set; }
    }
}