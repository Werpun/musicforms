﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MusicForms.DTO
{
    public class CreateFormDTO
    {
        public string FormTitle { get; set; }
        public int FormTypeId { get; set; }
        public Guid UserId { get; set; }
        public Guid GroupId { get; set; }
    }
}