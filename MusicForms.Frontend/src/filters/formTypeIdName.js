import Vue from 'vue'

export default Vue.filter('formTypeIdName', function (formTypeId) {
    if (!formTypeId) return;
    if (formTypeId == 1) return "Quiz"
    if (formTypeId == 2) return "Survey"
})