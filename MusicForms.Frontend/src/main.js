import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router'
import BootstrapVue from 'bootstrap-vue'
import VueAwesome from 'vue-awesome'
import 'vue-awesome/icons'
import Icon from 'vue-awesome/components/Icon'
import VueProgressBar from 'vue-progressbar'
import VueDebounce from 'vue-debounce'

import Login from '@/components/Login'
import Register from '@/components/Register'
import Home from '@/components/Home'
import Forms from '@/components/Forms'
import Groups from '@/components/Groups'
import DisplayGroup from '@/components/DisplayGroup'
import DisplayForm from '@/components/DisplayForm'
import EditForm from '@/components/EditForm'
import Profile from '@/components/Profile'
import FormStats from '@/components/FormStats'
import Admin from '@/components/Admin'

import "@/filters/formatDate.js"
import "@/filters/formTypeIdName.js"

import HttpClient from "@/services/axios.js"

const bus = new Vue()
Vue.prototype.$http = new HttpClient(bus)

Vue.prototype.$bus = bus

const options = {
  color: '#FFC107',
  failedColor: '#F44336',
  thickness: '5px',
}
Vue.use(VueProgressBar, options)

Vue.config.productionTip = false
Vue.use(VueRouter)
Vue.use(BootstrapVue)
Vue.use(VueAwesome)
Vue.use(VueDebounce)
Vue.component('icon', Icon)

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/', component: Home
    },
    {
      path: '/login', component: Login
    },
    {
      path: '/register', component: Register,
    },
    {
      path: '/forms', component: Forms,
      meta: { requiresAuth: true }
    },
    {
      path: '/groups', component: Groups,
      meta: { requiresAuth: true }
    },
    {
      path: '/groups/:id', component: DisplayGroup,
      meta: { requiresAuth: true }
    },
    {
      path: '/display/:id', component: DisplayForm,
      meta: { requiresAuth: true }
    },
    {
      path: '/edit/:id', component: EditForm,
      meta: { requiresAuth: true, requiresTeacher: true }
    },
    {
      name: 'profile',
      path: '/profile/:id', component: Profile,
      meta: { requiresAuth: true },
      props: true
    },
    {
      path: '/formStats/:id', component: FormStats,
      meta: { requiresAuth: true },
      props: true
    },
    {
      path: '/admin', component: Admin,
      meta: { requiresAuth: true, requiresAdmin: true },
    },
  ]
})

router.beforeEach((to, from, next) => {
  if (to.meta.requiresAuth) {
    const token = localStorage.getItem('token');
    if (token && window.user) {
      if (to.meta.requiresAdmin) {
        if (window.user.PermissionId === 1) {
          next();
        }
        else {
          bus.$emit('error', "Insufficient permission")
        }
      }
      else if (to.meta.requiresTeacher) {
        if (window.user.PermissionId <= 2) {
          next();
        }
        else {
          bus.$emit('error', "Insufficient permission")
        }
      }
      else next();
    }
    else {
      next({ path: '/login' });
    }
  }
  else {
    next();
  }
})


/* eslint-disable no-new */
export default new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
