import axios from 'axios';

const HttpClient = (bus) => {
  let instance = axios.create({
    // baseURL: 'http://localhost:51134/api/',
    baseURL: 'https://musicformsapi.azurewebsites.net/api/',
    headers: {
      'Content-Type': 'application/json',
    }
  })

  instance.interceptors.request.use(function (config) {
    const token = localStorage.getItem('token');
    config.headers.Authorization = token ? `Bearer ${token}` : '';
    VueProgressBarEventBus.$Progress.start()
    return config;
  });

  instance.interceptors.response.use((response) => {
    if (response.status === 200) {
      VueProgressBarEventBus.$Progress.finish();
    }

    return response;
  }, (error) => {
    VueProgressBarEventBus.$Progress.fail();
    bus.$emit('error', error.response.data.Message || 'Unexpected error occured');

    return Promise.reject(error);
  });

  return instance;
}

export default HttpClient;