-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------
IF OBJECT_ID(N'[dbo].[FK_Form_FormType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Form] DROP CONSTRAINT [FK_Form_FormType];
GO
IF OBJECT_ID(N'[dbo].[FK_Form_User]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Form] DROP CONSTRAINT [FK_Form_User];
GO
IF OBJECT_ID(N'[dbo].[FK_Form_Group]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Form] DROP CONSTRAINT [FK_Form_Group];
GO
IF OBJECT_ID(N'[dbo].[FK_Question_ContentBlock]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Question] DROP CONSTRAINT [FK_Question_ContentBlock];
GO
IF OBJECT_ID(N'[dbo].[FK_Question_Form]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Question] DROP CONSTRAINT [FK_Question_Form];
GO
IF OBJECT_ID(N'[dbo].[FK_Answer_Form]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Answer] DROP CONSTRAINT [FK_Answer_Form];
GO
IF OBJECT_ID(N'[dbo].[FK_Answer_Question]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Answer] DROP CONSTRAINT [FK_Answer_Question];
GO
IF OBJECT_ID(N'[dbo].[FK_Answer_User]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Answer] DROP CONSTRAINT [FK_Answer_User];
GO
IF OBJECT_ID(N'[dbo].[FK_User_Permission]', 'F') IS NOT NULL
	ALTER TABLE [dbo].[User] DROP CONSTRAINT [FK_User_Permission];
GO
IF OBJECT_ID(N'[dbo].[FK_UserGroup_User]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserGroup] DROP CONSTRAINT [FK_UserGroup_User];
GO
IF OBJECT_ID(N'[dbo].[FK_UserGroup_Group]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserGroup] DROP CONSTRAINT [FK_UserGroup_Group];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Form]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Form];
GO
IF OBJECT_ID(N'[dbo].[FormType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[FormType];
GO
IF OBJECT_ID(N'[dbo].[ContentBlock]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ContentBlock];
GO
IF OBJECT_ID(N'[dbo].[Question]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Question];
GO
IF OBJECT_ID(N'[dbo].[Answer]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Answer];
GO
IF OBJECT_ID(N'[dbo].[User]', 'U') IS NOT NULL
    DROP TABLE [dbo].[User];
GO
IF OBJECT_ID(N'[dbo].[Group]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Group];
GO
IF OBJECT_ID(N'[dbo].[UserGroup]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UserGroup];
GO
IF OBJECT_ID(N'[dbo].[Permission]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Permission];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Form'
CREATE TABLE [dbo].[Form] (
    [Id] uniqueidentifier DEFAULT NEWID() NOT NULL,
    [Title] nvarchar(255) NOT NULL,
    [FormTypeId] bigint NOT NULL,
    [Created] datetime NOT NULL CONSTRAINT [Form_Default_Created] DEFAULT (getutcdate()),
	[GroupId] uniqueidentifier NOT NULL,
    [AvailableFrom] datetime NULL,
    [AvailableTo] datetime NULL,
	[UserId] uniqueidentifier NOT NULL
);
GO

-- Creating table 'FormType'
CREATE TABLE [dbo].[FormType] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [DisplayName] nvarchar(255) NOT NULL,
    [InternalName] nvarchar(255) NOT NULL
);
GO

-- Creating table 'Question'
CREATE TABLE [dbo].[Question] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
	[FormId] uniqueidentifier NOT NULL,
    [ContentBlockId] bigint NOT NULL,
    [QuestionText] nvarchar(255) NOT NULL,
    [OrderNumber] int NOT NULL,
    [AvailableAnswers] nvarchar(255) NULL,
    [CorrectAnswer] nvarchar(255) NULL
);
GO

-- Creating table 'ContentBlock'
CREATE TABLE [dbo].[ContentBlock] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [Type] nvarchar(255) NOT NULL,
    [Name] nvarchar(max) NOT NULL
);
GO

-- Creating table 'Answer'
CREATE TABLE [dbo].[Answer] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [FormId] uniqueidentifier NOT NULL,
    [QuestionId] bigint NOT NULL,
    [UserId] uniqueidentifier NOT NULL,
    [GivenAnswer] nvarchar(255) NULL,
	[IsCorrect] bit NOT NULL
);
GO

-- Creating table 'User'
CREATE TABLE [dbo].[User] (
	[Id] uniqueidentifier DEFAULT NEWID() NOT NULL,
	[FirstName] nvarchar(255) NOT NULL,
	[LastName] nvarchar(255) NOT NULL,
	[Email] nvarchar(255) NOT NULL,
	[Password] nvarchar(255) NOT NULL,
	[PermissionId] bigint NOT NULL
);
GO

-- Creating table 'UserGroup'
CREATE TABLE [dbo].[UserGroup] (
	[UserId] uniqueidentifier NOT NULL,
	[GroupId] uniqueidentifier NOT NULL,
);
GO

-- Creating table 'Group'
CREATE TABLE [dbo].[Group] (
	[Id] uniqueidentifier DEFAULT NEWID() NOT NULL,
	[Name] nvarchar(255) NOT NULL,
	[TeacherId] uniqueidentifier NOT NULL
);
GO

-- Creating table 'Permission'
CREATE TABLE [dbo].[Permission] (
	[Id] bigint IDENTITY(1,1) NOT NULL,
	[Name] nvarchar(255) NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Form'
ALTER TABLE [dbo].[Form]
ADD CONSTRAINT [PK_Form]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'FormType'
ALTER TABLE [dbo].[FormType]
ADD CONSTRAINT [PK_FormType]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Question'
ALTER TABLE [dbo].[Question]
ADD CONSTRAINT [PK_Question]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ContentBlock'
ALTER TABLE [dbo].[ContentBlock]
ADD CONSTRAINT [PK_ContentBlock]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Answer'
ALTER TABLE [dbo].[Answer]
ADD CONSTRAINT [PK_Answer]
	PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'User'
ALTER TABLE [dbo].[User]
ADD CONSTRAINT [PK_User]
	PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Group'
ALTER TABLE [dbo].[Group]
ADD CONSTRAINT [PK_Group]
	PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'UserGroup'
ALTER TABLE [dbo].[UserGroup]
ADD CONSTRAINT [PK_UserGroup]
	PRIMARY KEY ([UserId], [GroupId]);
GO

-- Creating primary key on [Id] in table 'Permission'
ALTER TABLE [dbo].[Permission]
ADD CONSTRAINT [PK_Permission]
	PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [FormTypeId] in table 'Form'
ALTER TABLE [dbo].[Form]
ADD CONSTRAINT [FK_Form_FormType]
    FOREIGN KEY ([FormTypeId])
    REFERENCES [dbo].[FormType]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [UserId] in table 'Form'
ALTER TABLE [dbo].[Form]
ADD CONSTRAINT [FK_Form_User]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[User]
        ([Id])
GO

-- Creating foreign key on [GroupId] in table 'Form'
ALTER TABLE [dbo].[Form]
ADD CONSTRAINT [FK_Form_Group]
    FOREIGN KEY ([GroupId])
    REFERENCES [dbo].[Group]
        ([Id])
GO

-- Creating foreign key on [ContentBlockId] in table 'Question'
ALTER TABLE [dbo].[Question]
ADD CONSTRAINT [FK_Question_ContentBlock]
    FOREIGN KEY ([ContentBlockId])
    REFERENCES [dbo].[ContentBlock]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [FormId] in table 'Question'
ALTER TABLE [dbo].[Question]
ADD CONSTRAINT [FK_Question_Form]
    FOREIGN KEY ([FormId])
    REFERENCES [dbo].[Form]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [FormId] in table 'Answer'
ALTER TABLE [dbo].[Answer]
ADD CONSTRAINT [FK_Answer_Form]
    FOREIGN KEY ([FormId])
    REFERENCES [dbo].[Form]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [QuestionId] in table 'Answer'
ALTER TABLE [dbo].[Answer]
ADD CONSTRAINT [FK_Answer_Question]
    FOREIGN KEY ([QuestionId])
    REFERENCES [dbo].[Question]
        ([Id])
GO

-- Creating foreign key on [UserId] in table 'Answer'
ALTER TABLE [dbo].[Answer]
ADD CONSTRAINT [FK_Answer_User]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[User]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [PermissionId] in table 'User'
ALTER TABLE [dbo].[User]
ADD CONSTRAINT [FK_User_Permission]
	FOREIGN KEY ([PermissionId])
	REFERENCES [dbo].[Permission]
		([Id])
GO

-- Creating foreign key on [UserId] in table 'UserGroup'
ALTER TABLE [dbo].[UserGroup]
ADD CONSTRAINT [FK_UserGroup_User]
	FOREIGN KEY ([UserId])
	REFERENCES [dbo].[User]
		([Id])
GO

-- Creating foreign key on [GroupId] in table 'UserGroup'
ALTER TABLE [dbo].[UserGroup]
ADD CONSTRAINT [FK_UserGroup_Group]
	FOREIGN KEY ([GroupId])
	REFERENCES [dbo].[Group]
		([Id])
GO

-- --------------------------------------------------
-- Creating default values
-- --------------------------------------------------

-- Permissions and admin
DECLARE @AdminId uniqueidentifier = NEWID()

INSERT INTO [dbo].[Permission] ([Name]) VALUES ('Admin')
INSERT INTO [dbo].[Permission] ([Name]) VALUES ('Teacher')
INSERT INTO [dbo].[Permission] ([Name]) VALUES ('Student')
INSERT INTO [dbo].[Permission] ([Name]) VALUES ('Free user')

INSERT INTO [dbo].[User]
           ([Id]
		   ,[FirstName]
           ,[LastName]
           ,[Email]
           ,[Password]
		   ,[PermissionId])
     VALUES
           (@AdminId
		   ,'admin'
           ,'admin'
           ,'admin@gmail.com'
           ,'admin'
		   ,1)

-- Form types
INSERT INTO [dbo].[FormType]
           ([DisplayName]
           ,[InternalName])
     VALUES
           ('Quiz'
           ,'quiz')

INSERT INTO [dbo].[FormType]
           ([DisplayName]
           ,[InternalName])
     VALUES
           ('Survey'
           ,'survey')

-- Content blocks
INSERT INTO [dbo].[ContentBlock]
           ([Type]
           ,[Name])
     VALUES
           ('basic'
           ,'SingleChoice')

INSERT INTO [dbo].[ContentBlock]
           ([Type]
           ,[Name])
     VALUES
           ('basic'
           ,'MultipleChoice')

INSERT INTO [dbo].[ContentBlock]
           ([Type]
           ,[Name])
     VALUES
           ('musical'
           ,'Scales')

INSERT INTO [dbo].[ContentBlock]
           ([Type]
           ,[Name])
     VALUES
           ('musical'
           ,'Intervals')

-- --------------------------------------------------
-- End of script
-- --------------------------------------------------